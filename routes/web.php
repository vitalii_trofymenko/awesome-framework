<?php

$route->get('/', 'App\Controllers\IndexController::index')->setName('index');

$route->group('auth', function ($route) {
    $route->get('login', 'App\Controllers\Auth\LoginController::index')->setName('auth.login');
    $route->post('login', 'App\Controllers\Auth\LoginController::login');

    $route->get('register', 'App\Controllers\Auth\RegisterController::index')->setName('auth.register');
    $route->post('register', 'App\Controllers\Auth\RegisterController::register');
})->middleware($container->get(App\Middleware\Guest::class));

$route->group('', function ($route) {
    $route->get('/profile', 'App\Controllers\ProfileController::index')->setName('profile');

    $route->post('/auth/logout', 'App\Controllers\Auth\LogoutController::logout')->setName('auth.logout');
})->middleware($container->get(App\Middleware\Authenticated::class));
