<?php

namespace App\Services;

use App\Repositories\UserRepositoryInterface;

class UserService extends BaseService
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(array $attributes)
    {
        return $this->userRepository->create($attributes);
    }
}
