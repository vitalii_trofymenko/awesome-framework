<?php

namespace App\Controllers\Auth;

use App\Auth\Auth;
use App\Controllers\Controller;
use App\Session\Flash;
use App\Views\View;
use League\Route\RouteCollection;

class LoginController extends Controller
{
    protected View $view;
    protected Auth $auth;
    protected RouteCollection $route;
    protected Flash $flash;

    public function __construct(View $view, Auth $auth, RouteCollection $route, Flash $flash)
    {
        $this->view = $view;
        $this->auth = $auth;
        $this->route = $route;
        $this->flash = $flash;
    }

    public function index($request, $response)
    {
        return $this->view->render($response, 'auth/login.twig');
    }

    public function login($request, $response)
    {
        $data = $this->validate($request, [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (!$this->auth->attempt($data['email'], $data['password'], $data['remember'] ?? false)) {
            $this->flash->now('error', 'Unauthorized.');

            return redirect($request->getUri()->getPath());
        }

        return redirect($this->route->getNamedRoute('profile')->getPath());
    }
}
