<?php

namespace App\Controllers\Auth;

use App\Auth\Auth;
use App\Controllers\Controller;
use App\Models\User;
use App\Services\UserService;
use App\Views\View;
use League\Route\RouteCollection;

class RegisterController extends Controller
{
    protected View $view;
    protected RouteCollection $route;
    protected Auth $auth;
    protected UserService $userService;

    public function __construct(View $view, RouteCollection $route, Auth $auth, UserService $userService)
    {
        $this->view = $view;
        $this->route = $route;
        $this->auth = $auth;
        $this->userService = $userService;
    }

    public function index($request, $response)
    {
        return $this->view->render($response, 'auth/register.twig');
    }

    public function register($request)
    {
        $data = $this->validate($request, [
            'email' => ['required', 'email', ['exists', User::class]],
            'name' => ['required'],
            'password' => ['required'],
            'password_confirmation' => ['required', ['equals', 'password']],
        ]);

        $this->userService->create($data);

        if (!$this->auth->attempt($data['email'], $data['password'])) {
            return redirect('/');
        }

        return redirect($this->route->getNamedRoute('index')->getPath());
    }
}
