<?php

namespace App\Controllers;

use App\Views\View;

class IndexController
{
    protected $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function index($request, $response)
    {
        return $this->view->render($response, 'index.twig');
    }
}
