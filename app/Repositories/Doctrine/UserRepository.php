<?php

namespace App\Repositories\Doctrine;

use App\Auth\Hashing\Hasher;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Doctrine\ORM\EntityManager;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected User $model;
    protected EntityManager $db;
    protected Hasher $hash;

    public function __construct(Hasher $hash, EntityManager $db)
    {
        $this->model = new User();
        $this->db = $db;
        $this->hash = $hash;
    }

    public function create(array $attributes)
    {
        $this->model->fill([
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'password' => $this->hash->create($attributes['password'])
        ]);
        $this->save();

        return $this->model;
    }

    public function save()
    {
        $this->db->persist($this->model);
        $this->db->flush();
    }
}