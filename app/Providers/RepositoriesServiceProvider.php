<?php

namespace App\Providers;

use App\Auth\Hashing\Hasher;
use App\Repositories\Doctrine\UserRepository;
use App\Repositories\UserRepositoryInterface;
use Doctrine\ORM\EntityManager;
use League\Container\ServiceProvider\AbstractServiceProvider;

class RepositoriesServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        UserRepositoryInterface::class
    ];

    public function register()
    {
        $container = $this->getContainer();

        $container->share(UserRepositoryInterface::class, fn() => new UserRepository(
            $container->get(Hasher::class),
            $container->get(EntityManager::class)
        ));
    }
}
